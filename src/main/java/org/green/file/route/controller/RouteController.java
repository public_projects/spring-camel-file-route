package org.green.file.route.controller;

import org.apache.camel.Route;
import org.green.file.route.config.RouteConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

@Controller
public class RouteController {
    @Autowired
    private RouteConfig routeConfig;

    @GetMapping("/get/file/route/urls")
    public @ResponseBody
    ResponseEntity<Map<String, String>> fileRouteUrl() {
        Map urls = new HashMap<>(2);
        urls.put("from", routeConfig.getFrom());
        urls.put("to", routeConfig.getTo());
        return new ResponseEntity<>(urls, HttpStatus.OK);
    }
}
