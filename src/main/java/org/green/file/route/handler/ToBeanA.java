package org.green.file.route.handler;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;


@Service
@Slf4j
public class ToBeanA {
    public boolean handleBeanA(String fileName) {
        log.info("File e: getFilename: {}", fileName);
        return true;
    }
}