package org.green.file.route.handler;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.Random;


@Service
@Slf4j
public class BeanE {
    Random random = new Random();

    public boolean handleBeanE(String fileName) {
        boolean valid = random.nextBoolean();
        log.info("File e: getFilename: {}, return value: {}", fileName, valid);
        return valid;
    }
}
