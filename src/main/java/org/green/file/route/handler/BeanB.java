package org.green.file.route.handler;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.List;


@Service
@Slf4j
public class BeanB {
    public void handleBeanB(List<File> file) {
        log.info("File b: {}", file!= null? file.size(): null);
    }
}
