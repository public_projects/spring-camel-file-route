package org.green.file.route.handler;

import lombok.extern.slf4j.Slf4j;
import org.apache.camel.Exchange;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class ToBeanAExchange {
    public void handleExchange(Exchange exchange){
        log.info("Exchange {}: ", exchange.getExchangeId());
    }
}
