package org.green.file.route.handler;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.List;


@Service
@Slf4j
public class BeanC {
    public boolean handleBeanB(File file) {
        log.info("File c: {}", file != null ? file.getName(): null);

        if(file.getName().contains("ag03") || file.getName().contains("agmulti") ){
            log.error("***");
            log.error("File c: {} FOUND! We stop here!", file != null ? file.getName(): null);
            log.error("***");
            return false;
        }
        return true;
    }
}
