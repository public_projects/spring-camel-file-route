package org.green.file.route.handler;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class BeanAB {

    public void add(String a){
        log.info("a: {}", a);
    }
}
