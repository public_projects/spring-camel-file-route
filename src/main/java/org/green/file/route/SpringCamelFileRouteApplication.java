package org.green.file.route;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringCamelFileRouteApplication {
	public static void main(String[] args) {
		SpringApplication.run(SpringCamelFileRouteApplication.class, args);
	}
}
