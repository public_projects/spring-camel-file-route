package org.green.file.route.builder;

import lombok.extern.slf4j.Slf4j;
import org.apache.camel.builder.RouteBuilder;
import org.green.file.route.config.RouteConfig;
import org.green.file.route.handler.BeanA;
import org.green.file.route.handler.BeanB;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


//@Component
@Slf4j
public class FileRouteToBean extends RouteBuilder {
    @Autowired
    private RouteConfig routeConfig;

    @Autowired
    private BeanA beanA;

    @Autowired
    private BeanB beanB;

    @Override
    public void configure() throws Exception {
//        from("file:/home/greenhon/Documents/t5/a-frm?noop=true").streamCaching().to("file:/home/greenhon/Documents/t5/b-toTemp");
        from("file:" + routeConfig.getFrom() + "?noop=true").streamCaching().bean(beanA).bean(beanB);
    }
}