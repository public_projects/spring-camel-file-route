package org.green.file.route.builder;

import lombok.extern.slf4j.Slf4j;
import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.builder.ExchangeBuilder;
import org.apache.camel.builder.RouteBuilder;
import org.green.file.route.handler.ToBeanA;
import org.green.file.route.handler.ToBeanAExchange;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

//@Component
@Slf4j
public class DirectRouteWithFilter extends RouteBuilder {

    @Autowired
    private ToBeanAExchange toBeanAExchange;

    @Autowired
    private CamelContext camelContext;

    @Override
    public void configure() throws Exception {

//        ProducerTemplate producerTemplate = camelContext.createProducerTemplate();
//        Exchange exchange = new ExchangeBuilder(camelContext).withBody("This is exchange 1").build();
//        producerTemplate.send("direct:start", exchange);

        from("direct:start")
//                .filter().method(toBeanA, "handleBeanE(${body.getFileName})")
                .bean(toBeanAExchange);
    }
}
