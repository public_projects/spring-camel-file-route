package org.green.file.route.builder;

import org.apache.camel.CamelContext;
import org.apache.camel.FluentProducerTemplate;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.builder.RouteBuilder;
import org.green.file.route.config.RouteConfig;
import org.green.file.route.handler.BeanAB;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RouteDataCreator  extends RouteBuilder {
    @Autowired
    private RouteConfig routeConfig;

    @Autowired
    private CamelContext camelContext;

    @Autowired
    private BeanAB beanAB;

    @Override
    public void configure() throws Exception {
        ProducerTemplate pt = camelContext.createProducerTemplate();
        pt.sendBody("direct://start", "Hellow!");

        from("direct://start").bean(beanAB).to("file:/home/greenhon/Documents/t5/b-toTemp");
    }
}