package org.green.file.route.builder;

import lombok.extern.slf4j.Slf4j;
import org.apache.camel.builder.RouteBuilder;
import org.green.file.route.config.RouteConfig;
import org.green.file.route.handler.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;


@Component
@Slf4j
public class FileRouteThroughFilterToBean2 extends RouteBuilder {
    @Autowired
    private RouteConfig routeConfig;

    @Autowired
    private BeanA beanA;

    @Autowired
    private BeanB beanB;

    @Autowired
    private BeanE beanE;

    @Override
    public void configure() throws Exception {
        from("file:" + routeConfig.getFrom() + "?noop=true")
                .filter().method(beanE, "handleBeanE(${body.getFileName})")
                .bean(beanA);

        List<String> users = new ArrayList<>();
        users.add("015S850795113234022006SMSISDN");
        users.add("015S850795113234022006SMSISDN");
        users.add("015S401998662971666004SIMEI");
        users.add("015S401998662971666004SIMEI");
        users.add("015S618324995869196004SIMSI");
        users.add("015S618324995869196004SIMSI");
    }
}